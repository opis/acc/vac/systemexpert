from org.csstudio.display.builder.runtime.script import ScriptUtil
from org.csstudio.display.builder.runtime.pv import PVFactory

path = "../../vpi/faceplates/vac_digitelqpce_vpi.bob"

macros = widget.getEffectiveMacros()
devicename = macros.getValue("DEVICENAME")
ch = macros.getValue("CH")
try:
    pumpPV = PVFactory.getPV("{}:Ch{}_DevNameR".format(devicename, ch))
    pump = pumpPV.read()
    PVFactory.releasePV(pumpPV)
    pump = pump.value

    dmacros = dict()
    for k in macros.getNames():
        dmacros[k] = macros.getValue(k)
    dmacros["CONTROLLER"] = devicename
    dmacros["DEVICENAME"] = pump

    ScriptUtil.openDisplay(widget, path, "STANDALONE", dmacros)
except Exception as e:
    ScriptUtil.getLogger().severe(str(e))
