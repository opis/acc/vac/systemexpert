PVUtil = org.csstudio.display.builder.runtime.script.PVUtil;
ScriptUtil = org.csstudio.display.builder.runtime.script.ScriptUtil;

var pvUndefined = 0;
var pvOpen      = 0;
var pvClosed    = 0;
var pvError     = 0;

var pvSymbol    = pvs[0];

var sum     = 0;
var isValid = 0;
var colorID = 0;

var debug = widget.getEffectiveMacros().getValue("DEBUG");
if (debug) {
	debug = debug[0];
	switch (debug) {
		case '1':
		case 'Y':
		case 'y':
		case 'T':
		case 't':
			debug = true;
			break;

		default:
			debug = false;
	}
}
else
	debug = false;

if (debug)
	Logger = org.csstudio.display.builder.runtime.script.ScriptUtil.getLogger();
else {
	Logger = new Object();
	Logger.info = function() {}
	Logger.warning = function() {}
	Logger.severe = function(text) { org.csstudio.display.builder.runtime.script.ScriptUtil.getLogger().severe(text);}
}

function log_pv(pv) {
	Logger.info(pv + ": " + PVUtil.getString(pv));
}

try {
	pvUndefined = 1 * PVUtil.getInt(pvs[1]);
	pvOpen      = 2 * PVUtil.getInt(pvs[2]);
	pvClosed    = 4 * PVUtil.getInt(pvs[3]);
	pvError     = 8 * PVUtil.getInt(pvs[4]);

	sum         = pvUndefined | pvOpen | pvClosed | pvError;
	isValid     = (sum & (sum - 1)) == 0 ? 1 : 0;

	log_pv(pvs[1]);
	log_pv(pvs[2]);
	log_pv(pvs[3]);
	log_pv(pvs[4]);

	if (pvError) {
		Logger.info(pvSymbol + ": ERROR");
		colorID = 4;
	} else if (pvUndefined) {
		Logger.info(pvSymbol + ": UNDEFINED");
		colorID = 1;
	} else if (isValid == 0) {
		Logger.severe(pvSymbol + ": Invalid combination");
	} else if (pvClosed) {
		Logger.info(pvSymbol + ": CLOSED");
		colorID = 3;
	} else if (pvOpen) {
		Logger.info(pvSymbol + ": OPEN");
		colorID = 2;
	} else
		Logger.severe(pvSymbol + ": Unknown combination:" + sum);
} catch (err) {
	Logger.severe("NO CONNECTION: " + err);
}


pvSymbol.write(colorID);
