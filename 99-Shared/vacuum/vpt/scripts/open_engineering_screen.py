from org.csstudio.display.builder.runtime.script import ScriptUtil

################################################################################
#
# This script opens the engineering screen for the turbo pump:
# * sets the `DEVICENAME` macro from the `CONTROLLER` macro
# * sets the path to the engineering screen based on the `CONTROLLER_TYPE` macro
#
# Input PVs:
#  no PVs needed
#
################################################################################

leybold  = "../../vept/faceplates/vac_ctrl_leyboldtd20.bob"
pfeiffer = "../../vept/faceplates/vac_ctrl_pfeiffertcp350.bob"

macros = widget.getEffectiveMacros()
macros.expandValues(macros)
controller = macros.getValue("CONTROLLER")
if controller is None:
    ScriptUtil.showErrorDialog(widget, "Cannot determine device name of pump controller")
else:
    uitype = macros.getValue("CONTROLLER_TYPE")
    try:
        if "tcp350" in uitype.lower():
            path = pfeiffer
        elif "td20" in uitype.lower():
            path = leybold
        else:
            path = leybold
            ScriptUtil.getLogger().severe("Cannot determine controller type: '{}', Falling back to Leybold TD20 faceplate".format(uitype))
    except Exception as e:
        path = leybold
        ScriptUtil.getLogger().severe(str(e))
        ScriptUtil.getLogger().severe("Falling back to Leybold TD20 faceplate")

    macros.add("DEVICENAME", controller)
    dmacros = dict()
    for k in macros.getNames():
        dmacros[k] = macros.getValue(k)

    ScriptUtil.openDisplay(widget, path, "STANDALONE", dmacros)
