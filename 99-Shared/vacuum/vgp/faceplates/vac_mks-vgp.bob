<?xml version="1.0" encoding="UTF-8"?>
<display version="2.0.0">
  <name>$(DEVICENAME)</name>
  <width>771</width>
  <height>690</height>
  <widget type="rectangle" version="2.0.0">
    <name>Gauge Background</name>
    <width>771</width>
    <height>690</height>
    <line_color>
      <color name="GROUP-BORDER" red="150" green="155" blue="151">
      </color>
    </line_color>
    <background_color>
      <color name="GROUP-BACKGROUND" red="200" green="205" blue="201">
      </color>
    </background_color>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Gauge</name>
    <text>$(DEVICENAME)</text>
    <width>771</width>
    <height>35</height>
    <font>
      <font name="Header 2" family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <foreground_color>
      <color name="WHITE" red="255" green="255" blue="255">
      </color>
    </foreground_color>
    <background_color>
      <color name="GROUP-BORDER" red="150" green="155" blue="151">
      </color>
    </background_color>
    <transparent>false</transparent>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Status</name>
    <text>XX @ Yy</text>
    <x>691</x>
    <y>5</y>
    <width>70</width>
    <height>25</height>
    <font>
      <font name="SMALL-MONO-BOLD" family="Source Code Pro" style="BOLD" size="14.0">
      </font>
    </font>
    <foreground_color>
      <color name="WHITE" red="255" green="255" blue="255">
      </color>
    </foreground_color>
    <background_color>
      <color name="ERROR" red="252" green="13" blue="27">
      </color>
    </background_color>
    <transparent>false</transparent>
    <horizontal_alignment>2</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
    <scripts>
      <script file="../../COMMON/scripts/gauge_status.py">
        <pv_name>$(DEVICENAME):ValidR</pv_name>
        <pv_name>$(DEVICENAME):SensorTypeR</pv_name>
        <pv_name>$(DEVICENAME):ChanR</pv_name>
      </script>
    </scripts>
    <tooltip>NA</tooltip>
  </widget>
  <widget type="group" version="2.0.0">
    <name>Group Relays</name>
    <x>10</x>
    <y>55</y>
    <width>526</width>
    <height>350</height>
    <style>3</style>
    <widget type="rectangle" version="2.0.0">
      <name>Relay Background</name>
      <width>526</width>
      <height>350</height>
      <line_color>
        <color name="BLUE-GROUP-BORDER" red="138" green="167" blue="167">
        </color>
      </line_color>
      <background_color>
        <color name="BLUE-GROUP-BACKGROUND" red="179" green="209" blue="209">
        </color>
      </background_color>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Relays Header</name>
      <text>RELAYS</text>
      <width>526</width>
      <height>30</height>
      <font>
        <font name="Header 2" family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
        </font>
      </font>
      <foreground_color>
        <color name="GRAY-TEXT" red="255" green="255" blue="255">
        </color>
      </foreground_color>
      <background_color>
        <color name="BLUE-GROUP-BORDER" red="138" green="167" blue="167">
        </color>
      </background_color>
      <transparent>false</transparent>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="embedded" version="2.0.0">
      <name>MKS Gauge Relay Labels</name>
      <file>../../COMMON/embedded/vac_gauge-mks-relay-labels.bob</file>
      <y>97</y>
      <width>80</width>
      <height>200</height>
      <resize>1</resize>
      <transparent>true</transparent>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Relay description</name>
      <text>Relay Description:</text>
      <x>2</x>
      <y>71</y>
      <width>82</width>
      <height>50</height>
      <horizontal_alignment>2</horizontal_alignment>
    </widget>
    <widget type="embedded" version="2.0.0">
      <name>MKS Gauge Relay Control 1</name>
      <macros>
        <RELAY>1</RELAY>
      </macros>
      <file>../../COMMON/embedded/vac_gauge-mks-relay-control.bob</file>
      <x>90</x>
      <y>40</y>
      <width>212</width>
      <resize>2</resize>
      <transparent>true</transparent>
    </widget>
    <widget type="polyline" version="2.0.0">
      <name>Relay Separator</name>
      <x>303</x>
      <y>45</y>
      <width>1</width>
      <height>290</height>
      <points>
        <point x="0.0" y="0.0">
        </point>
        <point x="0.0" y="290.0">
        </point>
      </points>
      <line_width>1</line_width>
      <line_color>
        <color name="BLUE-GROUP-BORDER" red="138" green="167" blue="167">
        </color>
      </line_color>
    </widget>
    <widget type="embedded" version="2.0.0">
      <name>MKS Gauge Relay Control 2</name>
      <macros>
        <RELAY>2</RELAY>
      </macros>
      <file>../../COMMON/embedded/vac_gauge-mks-relay-control.bob</file>
      <x>308</x>
      <y>40</y>
      <width>212</width>
      <resize>2</resize>
      <transparent>true</transparent>
    </widget>
  </widget>
  <widget type="group" version="2.0.0">
    <name>Group Pressure</name>
    <x>10</x>
    <y>425</y>
    <width>370</width>
    <height>120</height>
    <style>3</style>
    <widget type="rectangle" version="2.0.0">
      <name>Pressure Background</name>
      <width>370</width>
      <height>120</height>
      <line_color>
        <color name="BLUE-GROUP-BORDER" red="138" green="167" blue="167">
        </color>
      </line_color>
      <background_color>
        <color name="BLUE-GROUP-BACKGROUND" red="179" green="209" blue="209">
        </color>
      </background_color>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Pressure Header</name>
      <text>PRESSURE</text>
      <width>370</width>
      <height>30</height>
      <font>
        <font name="Header 2" family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
        </font>
      </font>
      <foreground_color>
        <color name="GRAY-TEXT" red="255" green="255" blue="255">
        </color>
      </foreground_color>
      <background_color>
        <color name="BLUE-GROUP-BORDER" red="138" green="167" blue="167">
        </color>
      </background_color>
      <transparent>false</transparent>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Pressure</name>
      <text>Pressure:</text>
      <y>40</y>
      <width>120</width>
      <height>35</height>
      <font>
        <font name="Default Bold" family="Source Sans Pro" style="BOLD" size="16.0">
        </font>
      </font>
      <horizontal_alignment>2</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="textupdate" version="2.0.0">
      <name>PressureR</name>
      <pv_name>$(DEVICENAME):PrsR</pv_name>
      <x>130</x>
      <y>40</y>
      <width>110</width>
      <height>35</height>
      <font>
        <font name="Default Bold" family="Source Sans Pro" style="BOLD" size="16.0">
        </font>
      </font>
      <format>2</format>
      <horizontal_alignment>2</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="textupdate" version="2.0.0">
      <name>PressureStatR</name>
      <pv_name>$(DEVICENAME):PrsStatR</pv_name>
      <x>250</x>
      <y>40</y>
      <width>110</width>
      <height>35</height>
      <font>
        <font name="Default Bold" family="Source Sans Pro" style="BOLD" size="16.0">
        </font>
      </font>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
      <scripts>
        <script file="../../COMMON/scripts/pressure_status.py">
          <pv_name>$(DEVICENAME):PrsStatR</pv_name>
          <pv_name>$(DEVICENAME):PrsR-STR</pv_name>
        </script>
      </scripts>
    </widget>
    <widget type="label" version="2.0.0">
      <name>ATM Calibration</name>
      <text>ATM Calibration:</text>
      <y>85</y>
      <width>120</width>
      <height>25</height>
      <horizontal_alignment>2</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
      <tooltip>Send an atmospheric pressure to perform ATM calibration</tooltip>
    </widget>
    <widget type="textentry" version="3.0.0">
      <name>ATM CalibS</name>
      <pv_name>$(DEVICENAME):ATMCalibPrsS</pv_name>
      <x>130</x>
      <y>85</y>
      <width>110</width>
      <height>25</height>
      <format>2</format>
    </widget>
    <widget type="textupdate" version="2.0.0">
      <name>ATM CalibR</name>
      <pv_name>$(DEVICENAME):ATMCalibPrs-RB</pv_name>
      <x>250</x>
      <y>85</y>
      <width>110</width>
      <height>25</height>
      <format>2</format>
      <horizontal_alignment>2</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
  </widget>
  <widget type="group" version="2.0.0">
    <name>Group Misc</name>
    <x>400</x>
    <y>425</y>
    <width>361</width>
    <height>185</height>
    <style>3</style>
    <widget type="rectangle" version="2.0.0">
      <name>Misc Background</name>
      <width>361</width>
      <height>185</height>
      <line_color>
        <color name="BLUE-GROUP-BORDER" red="138" green="167" blue="167">
        </color>
      </line_color>
      <background_color>
        <color name="BLUE-GROUP-BACKGROUND" red="179" green="209" blue="209">
        </color>
      </background_color>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Misc Header</name>
      <text>MISC</text>
      <width>361</width>
      <height>30</height>
      <font>
        <font name="Header 2" family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
        </font>
      </font>
      <foreground_color>
        <color name="GRAY-TEXT" red="255" green="255" blue="255">
        </color>
      </foreground_color>
      <background_color>
        <color name="BLUE-GROUP-BORDER" red="138" green="167" blue="167">
        </color>
      </background_color>
      <transparent>false</transparent>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Power Status</name>
      <text>Power:</text>
      <y>40</y>
      <width>135</width>
      <height>25</height>
      <horizontal_alignment>2</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="combo" version="2.0.0">
      <name>PowerStatusS</name>
      <pv_name>$(DEVICENAME):PowerS</pv_name>
      <x>145</x>
      <y>40</y>
      <height>25</height>
    </widget>
    <widget type="led" version="2.0.0">
      <name>PowerStatusR</name>
      <pv_name>$(DEVICENAME):Power-RB</pv_name>
      <x>251</x>
      <y>40</y>
      <width>100</width>
      <height>25</height>
      <off_color>
        <color name="LED-GREEN-OFF" red="90" green="110" blue="90">
        </color>
      </off_color>
      <on_color>
        <color name="LED-GREEN-ON" red="70" green="255" blue="70">
        </color>
      </on_color>
      <square>true</square>
      <labels_from_pv>true</labels_from_pv>
      <tooltip>$(pv_name)$(pv_value)</tooltip>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Autozero</name>
      <text>Autozero Channel:</text>
      <y>75</y>
      <width>135</width>
      <height>25</height>
      <horizontal_alignment>2</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="combo" version="2.0.0">
      <name>AutozeroS</name>
      <pv_name>$(DEVICENAME):AutozeroChanS</pv_name>
      <x>145</x>
      <y>75</y>
      <height>25</height>
    </widget>
    <widget type="textupdate" version="2.0.0">
      <name>AutozeroR</name>
      <pv_name>$(DEVICENAME):AutozeroChan-RB</pv_name>
      <x>251</x>
      <y>75</y>
      <height>25</height>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Gas Type</name>
      <text>Gas Type:</text>
      <y>110</y>
      <width>135</width>
      <height>25</height>
      <horizontal_alignment>2</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="combo" version="2.0.0">
      <name>GasTypeS</name>
      <pv_name>$(DEVICENAME):GasTypeS</pv_name>
      <x>145</x>
      <y>110</y>
      <height>25</height>
    </widget>
    <widget type="textupdate" version="2.0.0">
      <name>GasTypeR</name>
      <pv_name>$(DEVICENAME):GasType-RB</pv_name>
      <x>251</x>
      <y>110</y>
      <height>25</height>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="action_button" version="3.0.0">
      <name>Zero Gague</name>
      <actions>
        <action type="write_pv">
          <pv_name>$(pv_name)</pv_name>
          <value>1</value>
          <description>Zero the Gauge</description>
        </action>
      </actions>
      <pv_name>$(DEVICENAME):ZeroS</pv_name>
      <x>201</x>
      <y>145</y>
      <width>150</width>
      <tooltip>$(actions)</tooltip>
    </widget>
  </widget>
  <widget type="rectangle" version="2.0.0">
    <name>Separator</name>
    <x>5</x>
    <y>620</y>
    <width>761</width>
    <height>3</height>
    <line_color>
      <color name="GROUP-BORDER" red="150" green="155" blue="151">
      </color>
    </line_color>
    <background_color>
      <color name="GROUP-BORDER" red="150" green="155" blue="151">
      </color>
    </background_color>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Controller Detailed</name>
    <actions>
      <action type="open_display">
        <file>../../veg/faceplates/vac_ctrl_mks946_937b_controller.bob</file>
        <macros>
          <DEVICENAME>$(CONTROLLER)</DEVICENAME>
        </macros>
        <target>window</target>
        <description>Open Controller Screen</description>
      </action>
    </actions>
    <x>601</x>
    <y>630</y>
    <width>150</width>
    <height>50</height>
    <tooltip>$(actions)</tooltip>
  </widget>
</display>
