from org.csstudio.display.builder.runtime.script import ScriptUtil

################################################################################
#
# This script opens the engineering screen for the ion pump:
# * sets the `DEVICENAME` macro from the `vacPREFIX` macro
# * sets the path to the engineering screen based on the `CONTROLLER_TYPE` macro
#    (this is not strictly required yet; there is only one kind of ion pump)
#
# Input PVs:
#  no PVs needed
#
################################################################################

digitelqpce  = "../faceplates/vac_digitelqpce_vpi.bob"

macros = widget.getEffectiveMacros()
prefix = macros.getValue("vacPREFIX")
uitype = macros.getValue("CONTROLLER_TYPE")
try:
    if "digitelqpc" in uitype.lower():
        path = digitelqpce
    else:
        path = digitelqpce
        ScriptUtil.getLogger().severe("Cannot determine controller type: '{}', Falling back to Digitel QPCe faceplate".format(uitype))
except Exception as e:
    path = digitelqpce
    ScriptUtil.getLogger().severe(str(e))
    ScriptUtil.getLogger().severe("Falling back to Digitel QPCe faceplate")


# This faceplate is for the pump, not the controller so we use prefix here
macros.add("DEVICENAME", prefix)
dmacros = dict()
for k in macros.getNames():
    dmacros[k] = macros.getValue(k)

ScriptUtil.openDisplay(widget, path, "STANDALONE", dmacros)
